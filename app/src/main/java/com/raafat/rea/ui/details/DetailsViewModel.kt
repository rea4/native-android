package com.raafat.rea.ui.details

import androidx.lifecycle.viewModelScope
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.repositories.ListingRepository
import com.raafat.rea.ui.base.BaseViewModel

/**
 * Created by Raafat Alhmidi on 3/15/2021 @3:26 AM.
 */
class DetailsViewModel : BaseViewModel() {
    private val repository = ListingRepository(viewModelScope)
    val detailsLD = repository.listingDetailsLD

    init {
        detailsLD.observeForever{
            when {
                it.isLoading -> {
                    _isLoadingLiveData.postValue(true)
                }
                else -> _isLoadingLiveData.postValue(false)
            }
        }
    }

    fun loadDetails(listingId:String){
        repository.loadDetails(listingId)
    }

    fun invertSaved(listing: Listing){
        listing.saved = !listing.saved
        repository.updateListing(listing)
    }
}