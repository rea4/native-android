package com.raafat.rea.ui.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentHomeBinding
import com.raafat.rea.model.data.Listing
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.ui.list.ListFragmentDirections
import com.raafat.rea.utils.ItemClickListener

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    override fun createViewModel(): HomeViewModel =
        ViewModelProvider(this).get(HomeViewModel::class.java)

    override fun getResourceId(): Int = R.layout.fragment_home
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadNews()
        viewModel.loadRecentVisited()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolBar.setOnClickListener {
            navController.navigate(HomeFragmentDirections.actionNavigationHomeToSearchFragment())
        }
        binding.recentMoreTxtV.setOnClickListener {
            navController.navigate(HomeFragmentDirections.actionNavigationHomeToRecentFragment())
        }
    }

    override fun getOnItemClickListener(): ItemClickListener? = object : ItemClickListener {
        override fun onClick(listing: Listing) {
            viewModel.visitListing(listing)
            navController.navigate(
                HomeFragmentDirections.actionNavigationHomeToDetailsFragment(
                    listing.id
                )
            )
        }

        override fun onClick(listingId: String) {
            viewModel.visitListing(listingId)
            navController.navigate(
                HomeFragmentDirections.actionNavigationHomeToDetailsFragment(
                    listingId
                )
            )
        }
    }



}