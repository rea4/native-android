package com.raafat.rea.ui.list

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentListBinding
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/14/2021 @7:01 AM.
 */
class ListFragment : BaseFragment<FragmentListBinding, ListViewModel>() {

    override fun createViewModel(): ListViewModel =
        ViewModelProvider(this).get(ListViewModel::class.java)

    override fun getResourceId(): Int = R.layout.fragment_list
    override fun getTopBar(): CustomToolBar? = binding.topBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var searchTerm: SearchTerm? = null
        arguments?.let {
            searchTerm = ListFragmentArgs.fromBundle(it).searchTerm
        }
        searchTerm?.let {
            viewModel.loadListing(it)
        }
    }

    override fun getOnItemClickListener(): ItemClickListener? = object : ItemClickListener {
        override fun onClick(listing: Listing) {
            viewModel.visitListing(listing)
            navController.navigate(
                ListFragmentDirections.actionListFragmentToDetailsFragment(
                    listing.id
                )
            )
        }

        override fun onSave(listing: Listing) {
            viewModel.invertSaved(listing)
            binding.dataRV.adapter?.notifyItemChanged(
                viewModel.listingLD.value?.data?.indexOf(
                    listing
                ) ?: 0
            )
        }

        override fun onClick(listingId: String) {
            viewModel.visitListing(listingId)
            navController.navigate(
                ListFragmentDirections.actionListFragmentToDetailsFragment(
                    listingId
                )
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.queryTxtV.setOnClickListener {
            navController.navigate(ListFragmentDirections.actionListFragmentToSearchFragment())
        }
    }
}