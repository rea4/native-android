package com.raafat.rea.ui.list

import androidx.lifecycle.viewModelScope
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.model.repositories.ListingRepository
import com.raafat.rea.ui.base.BaseViewModel

/**
 * Created by Raafat Alhmidi on 3/14/2021 @7:00 AM.
 */
class ListViewModel:BaseViewModel() {
    private val repository = ListingRepository(viewModelScope)
    val listingLD = repository.listingsLD
    var searchTerm:SearchTerm? = null
    init {
        listingLD.observeForever{
            when {
                it.isLoading -> {
                    _isLoadingLiveData.postValue(true)
                }
                else -> _isLoadingLiveData.postValue(false)
            }
        }
    }
    fun loadListing(searchTerm: SearchTerm){
        this.searchTerm = searchTerm
        repository.loadListingList(searchTerm)
    }
    fun invertSaved(listing: Listing){
        listing.saved = !listing.saved
        repository.updateListing(listing)
    }

    fun visitListing(listing: Listing){
        listing.visitDate = System.currentTimeMillis()
        repository.updateListing(listing)
    }
    fun visitListing(listingId: String){
        repository.updateListing(listingId,System.currentTimeMillis())
    }
}