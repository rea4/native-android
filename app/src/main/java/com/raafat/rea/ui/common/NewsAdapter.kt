package com.raafat.rea.ui.common

import com.raafat.rea.R
import com.raafat.rea.model.data.News
import com.raafat.rea.ui.base.BaseAdapter
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/9/2021 @12:55 AM.
 */
class NewsAdapter(itemClickListener: ItemClickListener?) :
    BaseAdapter<News>(itemClickListener,R.layout.item_news) {

}