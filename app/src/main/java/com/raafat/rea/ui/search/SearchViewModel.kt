package com.raafat.rea.ui.search

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.viewModelScope
import com.raafat.rea.BR
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.model.repositories.SearchRepository
import com.raafat.rea.ui.base.BaseViewModel

/**
 * Created by Raafat Alhmidi on 3/12/2021 @1:26 AM.
 */
class SearchViewModel : BaseViewModel() {
    private var query = ""
    private val searchRepository = SearchRepository(viewModelScope)
    val searchHistoryLD = searchRepository.searchHistoryLiveData
    val suggestionHistoryLD = searchRepository.suggestedHistoryLiveData
    val suggestionLD = searchRepository.suggestedSearchLiveData

    fun loadHistory() {
        searchRepository.loadSearchHistory()
    }

    fun loadSuggestion(q: String) {
        searchRepository.loadSuggestions(q)
    }
    fun insertOrUpdateSearch(searchTerm: SearchTerm){
        searchRepository.insertSearchHistory(searchTerm)
    }

    @Bindable
    fun getQuery() = query

    fun setQuery(q: String) {
        if (q != query) {
            query = q
            notifyPropertyChanged(BR.query)
            loadSuggestion(query)
        }
    }

}