package com.raafat.rea.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentSearchBinding
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.ui.home.HomeViewModel
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/12/2021 @1:26 AM.
 */
class SearchFragment : BaseFragment<FragmentSearchBinding, SearchViewModel>() {
    override fun getResourceId(): Int = R.layout.fragment_search

    override fun createViewModel(): SearchViewModel =
        ViewModelProvider(this).get(SearchViewModel::class.java)

    override fun getTopBar(): CustomToolBar? = binding.topBar

    override fun getOnItemClickListener(): ItemClickListener? = object : ItemClickListener{
        override fun onClick(searchTerm: SearchTerm) {
            viewModel.insertOrUpdateSearch(searchTerm)
            navController.navigate(SearchFragmentDirections.actionSearchFragmentToListFragment(searchTerm))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadHistory()
        binding.selectStateTxtV.setOnClickListener {
            navController.navigate(SearchFragmentDirections.actionSearchFragmentToStateAndAreaFragment())
        }
    }

}