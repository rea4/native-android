package com.raafat.rea.ui.stateAndArea

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentStateAndAreaBinding
import com.raafat.rea.model.data.City
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.model.data.State
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/14/2021 @2:11 AM.
 */
class StateAndAreaFragment : BaseFragment<FragmentStateAndAreaBinding, StateAndAreaViewModel>(),View.OnClickListener {

    override fun getResourceId(): Int = R.layout.fragment_state_and_area

    override fun createViewModel(): StateAndAreaViewModel =
        ViewModelProvider(this).get(StateAndAreaViewModel::class.java)

    override fun getTopBar(): CustomToolBar? = binding.topBar
    override fun getOnItemClickListener(): ItemClickListener? = object : ItemClickListener {
        override fun onClick(city: City) {
            viewModel.setSelectedCity(city)
            navController.navigateUp()
        }

        override fun onClick(state: State) {
            viewModel.setSelectedState(state)
            navController.navigateUp()

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadStates("")

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.areaValueTxtV.setOnClickListener(this)
        binding.stateValueTxtV.setOnClickListener(this)
        binding.searchBtn.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        val id = p0?.id
        when (id){
            binding.areaValueTxtV.id -> {
                navController.navigate(
                    StateAndAreaFragmentDirections.actionStateAndAreaFragmentToSelectionFragment(
                        state = viewModel.getSelectedState(),
                        listener = getOnItemClickListener(),
                        city= viewModel.getSelectedCity()
                    )
                )
            }
            binding.stateValueTxtV.id ->{
                navController.navigate(
                    StateAndAreaFragmentDirections.actionStateAndAreaFragmentToSelectionFragment(
                        state = null,
                        listener = getOnItemClickListener()
                    )
                )
            }
            binding.searchBtn.id->{
                val selectedStateName = viewModel.getSelectedState()?.name
                var selectedCityName  = viewModel.getSelectedCity()?.name
                if(selectedCityName == "Any")
                    selectedCityName = null

                val type = if(selectedCityName == null) "State" else "City/Area"
                val addresLevel1 = if(selectedCityName == null) "" else selectedStateName
                val searchTerm = SearchTerm(null,selectedCityName?:selectedStateName?:"",type,addressLevel1 = addresLevel1,null,"ic_twotone_location_on_24",System.currentTimeMillis())
                viewModel.insertSearchTerm(searchTerm)
                navController.navigate(StateAndAreaFragmentDirections.actionStateAndAreaFragmentToListFragment(searchTerm))
            }
        }
    }

}