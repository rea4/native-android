package com.raafat.rea.ui.stateAndArea

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.raafat.rea.BR
import com.raafat.rea.model.data.City
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.model.data.State
import com.raafat.rea.model.repositories.SearchRepository
import com.raafat.rea.model.repositories.StatesRepository
import com.raafat.rea.ui.base.BaseViewModel
import java.io.Serializable

/**
 * Created by Raafat Alhmidi on 3/14/2021 @2:11 AM.
 */
class StateAndAreaViewModel : BaseViewModel(), Serializable {
    private var query = ""
    private val repository = StatesRepository(viewModelScope)
    private val searchRepository = SearchRepository(viewModelScope)
    val statesLD = repository.statesLD
    val citiesLD = repository.citiesLD
    private var selectedState: State? = null
    private var selectedCity: City? = null

    init {
        statesLD.observeForever {
            if (it.isSuccess) {
                selectedState = it.data?.firstOrNull { it.selected } ?: it.data?.firstOrNull()
                selectedCity = selectedState?.cities?.firstOrNull { it.selected }
                    ?: selectedState?.cities?.firstOrNull()
                selectedState = selectedState?.apply {
                    this.selected = false
                    repository.updateState(this)
                    it.data?.firstOrNull()
                }
                selectedCity = selectedCity?.apply {
                    this.selected = false
                    repository.updateCity(this)
                    selectedState?.cities?.firstOrNull()
                }
                selectedState?.selected = true
                selectedCity?.selected = true
                selectedCity?.let { it1 -> repository.updateCity(it1) }
                selectedState?.let { it1 -> repository.updateState(it1) }

                notifyPropertyChanged(BR.selectedCity)
                notifyPropertyChanged(BR.selectedState)
            }
        }
    }

    fun loadStates(q: String) {
        repository.loadStates(q)
    }

    fun insertSearchTerm(searchTerm: SearchTerm) {
        searchRepository.insertSearchHistory(searchTerm)
    }

    @Bindable
    fun getSelectedState() = selectedState

    @Bindable
    fun getSelectedCity() = selectedCity

    fun setSelectedState(state: State?) {
        selectedState?.selected = false
        selectedState?.let { repository.updateState(it) }
        selectedState = state
        selectedState?.selected = true
        selectedState?.let { repository.updateState(it) }
        notifyPropertyChanged(BR.selectedState)
        setSelectedCity(selectedState?.cities?.firstOrNull())

    }

    fun setSelectedCity(city: City?) {
        selectedCity?.selected = false
        selectedCity?.let { repository.updateCity(it) }
        selectedCity = city
        selectedCity?.selected = true
        selectedCity?.let { repository.updateCity(it) }
        notifyPropertyChanged(BR.selectedCity)
    }
}