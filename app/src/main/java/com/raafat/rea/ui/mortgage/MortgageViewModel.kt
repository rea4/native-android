package com.raafat.rea.ui.mortgage

import androidx.databinding.*
import com.raafat.rea.BR
import com.raafat.rea.ui.base.BaseViewModel
import com.raafat.rea.utils.calculateMortage
import java.text.DecimalFormat
import java.util.*
import kotlin.math.roundToInt

/**
 * Created by Raafat Alhmidi on 3/10/2021 @2:15 AM.
 */
class MortgageViewModel : BaseViewModel() {
    private val formatter: DecimalFormat = DecimalFormat("#,###,###")

    private var propertyPrice = 0
    private var downPayment = 0

    private var downPaymentPercent = 1000
    private var interestRate = 470
    private var loanTerm: Int = 35


    @Bindable
    fun getLoanAmount(): String = "RM "+formatter.format(propertyPrice - downPayment)

    @Bindable
    fun getMontlyAmount(): String =
        "RM "+formatter.format(calculateMortage(propertyPrice, downPayment, interestRate/100.0, loanTerm))

    @Bindable
    fun getPropertyPrice(): String = formatter.format(propertyPrice)

    @Bindable
    fun getDownPayment(): String = formatter.format(downPayment)

    @Bindable
    fun getDownPaymentPercent(): String = String.format("%.2f", downPaymentPercent/100.0)

    @Bindable
    fun getInterestRate(): String = String.format("%.2f", interestRate/100.0)

    fun setInterestRate(value: String) {
        var multiplier: Float
        kotlin.runCatching {
            val doubleValue = value.toDouble()
            multiplier = when {
                value.length>getInterestRate().length -> 10.0f
                value.length<getInterestRate().length -> 0.1f
                else -> 1.0f
            }

            return@runCatching doubleValue * multiplier
        }.onSuccess {
            var _it = it
            if(_it>100) {
                _it = (_it * 10).toInt() / 100.0
            }
            if((_it*100).toInt() != interestRate){
                interestRate = (_it*100).roundToInt()
                notifyPropertyChanged(BR.interestRate)
                notifyPropertyChanged(BR.montlyAmount)
                notifyPropertyChanged(BR.loanAmount)
            }else if(_it == 0.0 && value.length<4 || (_it!=100.0 && value.length ==6))
                notifyPropertyChanged(BR.interestRate)

        }.onFailure {
            if(value.isBlank())
                interestRate = 0
            notifyPropertyChanged(BR.interestRate)
            notifyPropertyChanged(BR.montlyAmount)
            notifyPropertyChanged(BR.loanAmount)
        }
    }

    fun setDownPaymentPercent(value: String) {
        var multiplier: Float
        kotlin.runCatching {
            val doubleValue = value.toDouble()
            multiplier = when {
                value.length>getDownPaymentPercent().length -> 10.0f
                value.length<getDownPaymentPercent().length -> 0.1f
                else -> 1.0f
            }

            return@runCatching doubleValue * multiplier
        }.onSuccess {
            var _it = it
            if(_it>100) {
                _it = (_it * 10).toInt() / 100.0
            }
            if((_it*100).toInt() != downPaymentPercent){
                downPaymentPercent = (_it*100).roundToInt()
                downPayment = (downPaymentPercent.toLong()*propertyPrice/10000.0).roundToInt()
                notifyPropertyChanged(BR.downPaymentPercent)
                notifyPropertyChanged(BR.downPayment)
                notifyPropertyChanged(BR.loanAmount)
                notifyPropertyChanged(BR.montlyAmount)
            }else if(_it == 0.0 && value.length<4 || (_it != 100.0 && value.length ==6)) {
                notifyPropertyChanged(BR.downPaymentPercent)
            }

        }.onFailure {
            if (value.isBlank()) {
                downPaymentPercent = 0
                downPayment = 0
            }
            notifyPropertyChanged(BR.downPaymentPercent)
            notifyPropertyChanged(BR.downPayment)
            notifyPropertyChanged(BR.loanAmount)
            notifyPropertyChanged(BR.montlyAmount)
        }


    }

    @Bindable
    fun getLoanTerm():String = loanTerm.toString()

    fun setLoanTerm(value: String){
        kotlin.runCatching {
            value.toInt()
        }.onSuccess {
            if(it != loanTerm){
                loanTerm = it
                notifyPropertyChanged(BR.loanTerm)
                notifyPropertyChanged(BR.montlyAmount)
                notifyPropertyChanged(BR.loanAmount)
            }
        }.onFailure {
            if(value.isBlank())
                loanTerm = 5
            notifyPropertyChanged(BR.loanTerm)
            notifyPropertyChanged(BR.montlyAmount)
            notifyPropertyChanged(BR.loanAmount)
        }
    }


    fun setPropertyPrice(value: String) {
        kotlin.runCatching {
            value.filterNot { it == ',' }.toInt()

        }.onSuccess {
            if (it != propertyPrice) {
                propertyPrice = it
                downPayment = (downPaymentPercent.toLong()*propertyPrice/10000.0).roundToInt()
                notifyPropertyChanged(BR.propertyPrice)
                notifyPropertyChanged(BR.downPayment)
                notifyPropertyChanged(BR.loanAmount)
                notifyPropertyChanged(BR.montlyAmount)

            }
        }.onFailure {
            if (value.isBlank()) {
                propertyPrice = 0
                downPayment = 0
            }
            notifyPropertyChanged(BR.propertyPrice)
            notifyPropertyChanged(BR.downPayment)
            notifyPropertyChanged(BR.loanAmount)
            notifyPropertyChanged(BR.montlyAmount)

        }
    }

    fun setDownPayment(value: String) {
        kotlin.runCatching {
            value.filterNot { it == ',' }.toInt()

        }.onSuccess {
            if (it != downPayment && downPayment < propertyPrice) {
                downPayment = it
                downPaymentPercent = (downPayment*10000)/propertyPrice
                notifyPropertyChanged(BR.downPayment)
                notifyPropertyChanged(BR.downPaymentPercent)
                notifyPropertyChanged(BR.loanAmount)
                notifyPropertyChanged(BR.montlyAmount)

            }
        }.onFailure {
            if (value.isBlank())
                downPayment = 0
            notifyPropertyChanged(BR.downPayment)
            notifyPropertyChanged(BR.downPaymentPercent)
            notifyPropertyChanged(BR.loanAmount)
            notifyPropertyChanged(BR.montlyAmount)

        }
    }





}