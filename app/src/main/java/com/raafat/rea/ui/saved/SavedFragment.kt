package com.raafat.rea.ui.saved

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentSavedBinding
import com.raafat.rea.model.data.Listing
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.ui.list.ListFragmentDirections
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/14/2021 @7:01 AM.
 */
class SavedFragment : BaseFragment<FragmentSavedBinding, SavedViewModel>() {

    override fun createViewModel(): SavedViewModel =
        ViewModelProvider(this).get(SavedViewModel::class.java)

    override fun getResourceId(): Int = R.layout.fragment_saved
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadSaved()
    }

    override fun getOnItemClickListener(): ItemClickListener? = object : ItemClickListener {
        override fun onClick(listing: Listing) {
            viewModel.visitListing(listing)
            navController.navigate(
                SavedFragmentDirections.actionNavigationSavedToDetailsFragment(
                    listing.id
                )
            )
        }

        override fun onSave(listing: Listing) {
            viewModel.invertSaved(listing)
            binding.dataRV.adapter?.notifyItemChanged(viewModel.savedLD.value?.data?.indexOf(listing)?:0)
        }

        override fun onClick(listingId: String) {
            viewModel.visitListing(listingId)
            navController.navigate(
                SavedFragmentDirections.actionNavigationSavedToDetailsFragment(
                    listingId
                )
            )
        }
    }

}