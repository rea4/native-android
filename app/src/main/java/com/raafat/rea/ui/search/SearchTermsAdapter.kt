package com.raafat.rea.ui.search

import com.raafat.rea.R
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.ui.base.BaseAdapter
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/12/2021 @1:29 AM.
 */
class SearchTermsAdapter(itemClickListener: ItemClickListener?,query:String?) : BaseAdapter<SearchTerm>(itemClickListener,
    R.layout.item_recent_search,query) {
}