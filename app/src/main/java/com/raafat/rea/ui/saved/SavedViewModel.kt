package com.raafat.rea.ui.saved

import androidx.lifecycle.viewModelScope
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.repositories.ListingRepository
import com.raafat.rea.ui.base.BaseViewModel

/**
 * Created by Raafat Alhmidi on 3/15/2021 @2:45 AM.
 */
class SavedViewModel:BaseViewModel() {
    private val repository = ListingRepository(viewModelScope)
    val savedLD = repository.listingsLD

    init {
        savedLD.observeForever{
            when {
                it.isLoading -> {
                    _isLoadingLiveData.postValue(true)
                }
                else -> _isLoadingLiveData.postValue(false)
            }
        }
    }

    fun loadSaved(){
        repository.loadSaved()
    }

    fun invertSaved(listing: Listing){
        listing.saved = !listing.saved
        repository.updateListing(listing)
    }

    fun visitListing(listing: Listing){
        listing.visitDate = System.currentTimeMillis()
        repository.updateListing(listing)
    }
    fun visitListing(listingId: String){
        repository.updateListing(listingId,System.currentTimeMillis())
    }
}