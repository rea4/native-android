package com.raafat.rea.ui.recent

import androidx.lifecycle.viewModelScope
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.repositories.ListingRepository
import com.raafat.rea.ui.base.BaseViewModel

/**
 * Created by Raafat Alhmidi on 3/15/2021 @2:45 AM.
 */
class RecentViewModel:BaseViewModel() {
    private val repository = ListingRepository(viewModelScope)
    val recentLD = repository.listingsLD

    init {
        recentLD.observeForever{
            when {
                it.isLoading -> {
                    _isLoadingLiveData.postValue(true)
                }
                else -> _isLoadingLiveData.postValue(false)
            }
        }
    }

    fun loadRecent(){
        repository.loadRecentVisited()
    }

    fun invertSaved(listing: Listing){
        listing.saved = !listing.saved
        repository.updateListing(listing)
    }
    fun visitListing(listing: Listing){
        listing.visitDate = System.currentTimeMillis()
        repository.updateListing(listing)
    }
    fun visitListing(listingId: String){
        repository.updateListing(listingId,System.currentTimeMillis())
    }
}