package com.raafat.rea.ui.stateSelection

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentSelectionBinding
import com.raafat.rea.model.data.City
import com.raafat.rea.model.data.State
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/14/2021 @3:04 AM.
 */
class SelectionFragment : BaseFragment<FragmentSelectionBinding, StateSelectionViewModel>() {
    private var state: State? = null
    private var city:City? = null
    private var _listener: ItemClickListener? = null
    override fun createViewModel(): StateSelectionViewModel =
        ViewModelProvider(this).get(StateSelectionViewModel::class.java)

    override fun getResourceId(): Int = R.layout.fragment_selection

    override fun getTopBar(): CustomToolBar? = binding.topBar

    override fun getOnItemClickListener(): ItemClickListener? = _listener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            SelectionFragmentArgs.fromBundle(it).let { args ->
                state = args.state
                _listener = args.listener
                city = args.city
            }
        }
        viewModel.setSelectedState(state)
        if (state == null)
            viewModel.loadStates("")
        else
            viewModel.loadCities("")
    }


}