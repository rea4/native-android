package com.raafat.rea.ui.recent

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentRecentBinding
import com.raafat.rea.model.data.Listing
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.ui.list.ListFragmentDirections
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/14/2021 @7:01 AM.
 */
class RecentFragment : BaseFragment<FragmentRecentBinding, RecentViewModel>() {
    override fun getTopBar(): CustomToolBar? = binding.topBar

    override fun createViewModel(): RecentViewModel =
        ViewModelProvider(this).get(RecentViewModel::class.java)

    override fun getResourceId(): Int = R.layout.fragment_recent
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadRecent()
    }

    override fun getOnItemClickListener(): ItemClickListener? = object : ItemClickListener {
        override fun onClick(listing: Listing) {
            viewModel.visitListing(listing)
            navController.navigate(
                RecentFragmentDirections.actionRecentFragmentToDetailsFragment(
                    listing.id
                )
            )
        }

        override fun onSave(listing: Listing) {
            viewModel.invertSaved(listing)
            binding.dataRV.adapter?.notifyItemChanged(
                viewModel.recentLD.value?.data?.indexOf(
                    listing
                ) ?: 0
            )
        }

        override fun onClick(listingId: String) {
            viewModel.visitListing(listingId)
            navController.navigate(
                RecentFragmentDirections.actionRecentFragmentToDetailsFragment(
                    listingId
                )
            )
        }
    }

}