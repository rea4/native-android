package com.raafat.rea.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.repositories.ListingRepository
import com.raafat.rea.model.repositories.NewsRepository
import com.raafat.rea.ui.base.BaseViewModel

class HomeViewModel : BaseViewModel() {
    private val newsRepository = NewsRepository(viewModelScope)
    private val listingRepository = ListingRepository(viewModelScope)
    val newsLiveData = newsRepository.newsLiveData
    val guideLiveData = newsRepository.guidesLiveData
    val recentListing = listingRepository.listingsLD
    val lifestyleLiveData = newsRepository.lifestyleLiveData

    init {
        newsLiveData.observeForever {
            _isLoadingLiveData.postValue(
                newsLiveData.value?.isLoading == true ||
                        guideLiveData.value?.isLoading == true ||
                        lifestyleLiveData.value?.isLoading == true
            )
        }
        guideLiveData.observeForever {
            _isLoadingLiveData.postValue(
                newsLiveData.value?.isLoading == true ||
                        guideLiveData.value?.isLoading == true ||
                        lifestyleLiveData.value?.isLoading == true
            )
        }
        lifestyleLiveData.observeForever {
            _isLoadingLiveData.postValue(
                newsLiveData.value?.isLoading == true ||
                        guideLiveData.value?.isLoading == true ||
                        lifestyleLiveData.value?.isLoading == true
            )
        }
    }

    fun loadNews() = newsRepository.loadNews()

    fun loadRecentVisited(){
        listingRepository.loadRecentVisited()
    }

    fun visitListing(listing: Listing){
        listing.visitDate = System.currentTimeMillis()
        listingRepository.updateListing(listing)
    }
    fun visitListing(listingId: String){
        listingRepository.updateListing(listingId,System.currentTimeMillis())
    }
}