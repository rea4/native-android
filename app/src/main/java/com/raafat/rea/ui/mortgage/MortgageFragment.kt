package com.raafat.rea.ui.mortgage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentMortgageBinding
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.ui.home.HomeViewModel
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.TopBarBtnListeners

/**
 * Created by Raafat Alhmidi on 3/10/2021 @2:15 AM.
 */
class MortgageFragment : BaseFragment<FragmentMortgageBinding, MortgageViewModel>() {

    override fun getResourceId(): Int = R.layout.fragment_mortgage

    override fun createViewModel(): MortgageViewModel = MortgageViewModel()

    override fun getTopBar(): CustomToolBar? = binding.topBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val price  = MortgageFragmentArgs.fromBundle(it).totalPrice
            viewModel.setPropertyPrice(price.toString())
        }
    }

}