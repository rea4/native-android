package com.raafat.rea.ui.common

import androidx.annotation.LayoutRes
import com.raafat.rea.R
import com.raafat.rea.model.data.Listing
import com.raafat.rea.ui.base.BaseAdapter
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/14/2021 @7:07 AM.
 */
class ListingAdapter(itemClickListener: ItemClickListener?, @LayoutRes itemLayout:Int = R.layout.item_listing) :
    BaseAdapter<Listing>(itemClickListener, itemLayout) {
}