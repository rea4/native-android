package com.raafat.rea.ui.stateSelection

import android.widget.BaseAdapter
import com.raafat.rea.R
import com.raafat.rea.model.data.State
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/14/2021 @3:17 AM.
 */
class StatesAdapter(itemClickListener: ItemClickListener?):com.raafat.rea.ui.base.BaseAdapter<State>(itemClickListener,
    R.layout.item_state)
