package com.raafat.rea.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.raafat.rea.BR
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/9/2021 @5:26 PM.
 */
abstract class BaseAdapter<T>(
    private val itemClickListener: ItemClickListener?,
    @LayoutRes private val resId: Int,
    private val query:String? = null
) :
    RecyclerView.Adapter<BaseAdapter<T>.BaseViewHolder>() {
    var data = listOf<T>()

    inner class BaseViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: T) {
            binding.setVariable(BR.item, item)
            binding.setVariable(BR.query,query)
            binding.setVariable(BR.listener, itemClickListener)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), resId, parent, false)

        return BaseViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(data[position])
        holder.itemView.setOnClickListener {
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int = data.size
}