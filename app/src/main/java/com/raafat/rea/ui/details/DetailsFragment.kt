package com.raafat.rea.ui.details

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.raafat.rea.R
import com.raafat.rea.databinding.FragmentDetailsBinding
import com.raafat.rea.ui.base.BaseFragment
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.ItemClickListener
import com.raafat.rea.utils.TopBarBtnListeners

/**
 * Created by Raafat Alhmidi on 3/15/2021 @3:27 AM.
 */
class DetailsFragment : BaseFragment<FragmentDetailsBinding, DetailsViewModel>(),View.OnClickListener {
    override fun getResourceId(): Int = R.layout.fragment_details
    override fun createViewModel(): DetailsViewModel =
        ViewModelProvider(this).get(DetailsViewModel::class.java)

    override fun getTopBar(): CustomToolBar? = binding.topBar
    override fun getTopBarListeners(): TopBarBtnListeners? = object : TopBarBtnListeners {
        override fun onFirstBtnClick() {
            viewModel.detailsLD.value?.data?.let { viewModel.invertSaved(it) }
        }

        override fun onSecondBtnClick() {
            super.onSecondBtnClick()
        }
    }

    override fun getOnItemClickListener(): ItemClickListener? = object : ItemClickListener {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var listingId: String? = null
        arguments?.let { 
            listingId = DetailsFragmentArgs.fromBundle(it).listingId
        }
        listingId?.let {
            viewModel.loadDetails(it)

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.mortgageLabelTxtV.setOnClickListener(this)
        binding.mortgageValueTxtV.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            binding.mortgageLabelTxtV.id , binding.mortgageValueTxtV.id -> {
                navController.navigate(DetailsFragmentDirections.actionDetailsFragmentToMortgageFragment(viewModel.detailsLD.value?.data?.prices?.get(0)?.max?:0 ))
            }
        }
    }
}