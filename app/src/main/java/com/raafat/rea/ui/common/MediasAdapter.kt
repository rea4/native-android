package com.raafat.rea.ui.common

import androidx.annotation.LayoutRes
import com.raafat.rea.R
import com.raafat.rea.model.data.Media
import com.raafat.rea.model.data.News
import com.raafat.rea.ui.base.BaseAdapter
import com.raafat.rea.utils.ItemClickListener

/**
 * Created by Raafat Alhmidi on 3/9/2021 @12:55 AM.
 */
class MediasAdapter(itemClickListener: ItemClickListener?, @LayoutRes itemLayout:Int) :
    BaseAdapter<Media>(itemClickListener,itemLayout) {

}