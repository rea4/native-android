package com.raafat.rea.ui.stateSelection

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.raafat.rea.BR
import com.raafat.rea.model.data.City
import com.raafat.rea.model.data.State
import com.raafat.rea.model.repositories.StatesRepository
import com.raafat.rea.ui.base.BaseViewModel
import java.io.Serializable

/**
 * Created by Raafat Alhmidi on 3/14/2021 @2:11 AM.
 */
class StateSelectionViewModel : BaseViewModel(), Serializable {
    private var query = ""
    private val repository = StatesRepository(viewModelScope)
    val statesLD = repository.statesLD
    val citiesLD = repository.citiesLD
    private var selectedState: State? = null
    private var selectedCity: City? = null


    fun loadStates(q: String) {
        repository.loadStates(q)
    }

    fun loadCities(q: String) {
        repository.loadCities(selectedState?.name ?: "", q)
    }

    @Bindable
    fun getQuery() = query

    fun setQuery(q: String) {
        if (q != query) {
            query = q
            notifyPropertyChanged(BR.query)
            if (selectedState == null)
                loadStates(query)
            else
                loadCities(query)
        }
    }

    @Bindable
    fun getSelectedState() = selectedState

    @Bindable
    fun getSelectedCity() = selectedCity

    fun setSelectedState(state: State?) {
        selectedState?.selected = false
        selectedState?.let { repository.updateState(it) }
        selectedState = state
        selectedState?.selected = true
        selectedState?.let { repository.updateState(it) }

        notifyPropertyChanged(BR.selectedState)
    }

    fun setSelectedCity(city: City?) {
        selectedCity?.selected = false
        selectedCity?.let { repository.updateCity(it) }
        selectedCity = city
        selectedCity?.selected = true
        selectedCity?.let { repository.updateCity(it) }

        notifyPropertyChanged(BR.selectedCity)
    }
}