package com.raafat.rea.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.BaseObservable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.raafat.rea.BR
import com.raafat.rea.utils.CustomToolBar
import com.raafat.rea.utils.ItemClickListener
import com.raafat.rea.utils.TopBarBtnListeners

/**
 * Created by Raafat Alhmidi on 3/9/2021 @10:37 PM.
 */
abstract class BaseFragment<B : ViewDataBinding, VM : BaseViewModel> : Fragment() {

    protected lateinit var navController: NavController

    @LayoutRes
    abstract fun getResourceId(): Int

    abstract fun createViewModel(): VM

    open fun getTopBar(): CustomToolBar? = null

    open fun getTopBarListeners(): TopBarBtnListeners? = null
    open fun getOnItemClickListener(): ItemClickListener? = null
    protected lateinit var viewModel: VM
    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = createViewModel()

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getResourceId(), container, false)
        binding.setVariable(BR.viewModel, viewModel)
        binding.setVariable(BR.listener, getOnItemClickListener())
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        getTopBar()?.let {
            it.navController = navController
            it.topBarBtnListeners = getTopBarListeners()
        }
    }


}