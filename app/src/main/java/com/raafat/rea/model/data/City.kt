package com.raafat.rea.model.data

import androidx.room.*
import java.io.Serializable

/**
 * Created by Raafat Alhmidi on 3/9/2021 @10:54 PM.
 */
@Entity(
    tableName = "cities_table",
    foreignKeys = [
        ForeignKey(
            entity = State::class,
            parentColumns = ["name"],
            childColumns = ["stateName"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("stateName"),
        Index("id")
    ]
)
data class City(
    @PrimaryKey(autoGenerate = true)
    val id:Int? = null,
    val name: String,
    var stateName: String?,
    var selected: Boolean = false
):Serializable
