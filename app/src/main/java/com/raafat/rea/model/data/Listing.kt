package com.raafat.rea.model.data

import androidx.room.*

/**
 * Created by Raafat Alhmidi on 3/9/2021 @11:51 PM.
 */
@Entity(
    tableName = "listings_table",
    indices = [
        Index("listingId", unique = true)
    ]
)
data class Listing(
    @PrimaryKey
    @ColumnInfo(name = "listingId")
    val id: String,
    val title: String,
    val kind: String,
    val shareLink: String,
    val active: Boolean,
    val tier: String,
    val propertyType: String,
    val updatedAt: String,
    val publishedAt: String?,
    val referenceCode: String,
    var visitDate:Long,
    var saved:Boolean
) {
    @Ignore
    var attributes: Attribute? = null

    @Ignore
    var listers: List<Lister>? = null

    @Ignore
    var organisations: List<Organisation>? = null

    @Ignore
    var address: Address? = null

    @Ignore
    val multilanguagePlace: MultilanguagePlace? = null

    @Ignore
    var prices: List<Price>? = null

    @Ignore
    var cover: Media? = null

    @Ignore
    var medias: List<Media>? = null

    @Ignore
    val channels: List<String>? = null
}
