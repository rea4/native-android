package com.raafat.rea.model.repositories.db.pojo

import androidx.room.Embedded
import androidx.room.Relation
import com.raafat.rea.model.data.*

/**
 * Created by Raafat Alhmidi on 3/13/2021 @6:54 PM.
 */

data class OrganisationDetails(
    @Embedded
    val organisation: Organisation,
    @Relation(
        parentColumn = "organisationId",
        entityColumn = "organisationId",
        entity = Contact::class
    )
    val contact: ContactDetails?,
    @Relation(
        parentColumn = "organisationId",
        entityColumn = "organisationId"
    )
    val logo: Media?,
    @Relation(
        parentColumn = "organisationId",
        entityColumn = "organisationId"
    )
    val address:Address?
){
    fun getAsOrganisation():Organisation{
        organisation.address = address
        organisation.logo = logo
        organisation.contact = contact?.getAsContact()
        return organisation
    }
}
