package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

/**
 * Created by Raafat Alhmidi on 3/11/2021 @10:50 PM.
 */

@Entity(
    tableName = "search_terms_table",
    indices = [
        Index(value = ["id","title","type"], unique = true)
    ]
)
data class SearchTerm(
    @PrimaryKey
    var id: Long?,
    val title: String,
    val type: String,
    val addressLevel1: String?,
    val addressLevel2: String?,
    val icon: String?,
    var searchDate: Long?
):Serializable {
    fun getAddress(): String {
        if (addressLevel1.isNullOrBlank())
            return ""
        if (addressLevel2.isNullOrBlank())
            return addressLevel1
        return "$addressLevel1, $addressLevel2"
    }

    fun getTypeInt(): Int {
        return when (type) {
            "State" -> 1
            "City/Area" -> 2
            null -> 3
            else -> 4
        }
    }

    fun getHtmlTitle(q: String?): String {
        var title: String = this.title
        title = title.replace("(?i)($q)".toRegex(), "<font color='red'>$1</font>")
        return title
    }

}