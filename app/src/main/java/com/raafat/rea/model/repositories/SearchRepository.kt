package com.raafat.rea.model.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.raafat.rea.IPropertyApp
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.model.repositories.db.IPropertyDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

/**
 * Created by Raafat Alhmidi on 3/12/2021 @1:14 AM.
 */
class SearchRepository(private val scope: CoroutineScope) {
    private val database = IPropertyDB.getDatabase(IPropertyApp.instance, scope)
    private val _suggestedHistory = MutableLiveData<APIResponse<List<SearchTerm>>>()
    private val _searchHistory = MutableLiveData<APIResponse<List<SearchTerm>>>()
    private val _suggestedSearch = MutableLiveData<APIResponse<List<SearchTerm>>>()

    val suggestedHistoryLiveData: LiveData<APIResponse<List<SearchTerm>>>
        get() = _suggestedHistory
    val suggestedSearchLiveData: LiveData<APIResponse<List<SearchTerm>>>
        get() = _suggestedSearch
    val searchHistoryLiveData: LiveData<APIResponse<List<SearchTerm>>>
        get() = _searchHistory

    private fun loadSearchHistoryFromDb() = flow<APIResponse<List<SearchTerm>>> {
        val list = database.searchTermsDao().loadHistory()
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    private fun loadSuggestedHistoryFromDb(q: String) = flow<APIResponse<List<SearchTerm>>> {
        emit(APIResponse.loading())
        kotlinx.coroutines.delay(200)
        val list = database.searchTermsDao().loadSuggestedHistory(q)
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    private fun loadSuggestionFromDb(q: String) = flow<APIResponse<List<SearchTerm>>> {
        emit(APIResponse.loading())
        kotlinx.coroutines.delay(200)
        val list = database.searchTermsDao().loadSuggestions(q)
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    fun loadSearchHistory() {
        scope.launch(Dispatchers.IO) {
            loadSearchHistoryFromDb().collect { _searchHistory.postValue(it) }
        }
    }

    fun insertSearchHistory(searchTerm: SearchTerm){
        scope.launch(Dispatchers.IO) {
            database.searchTermsDao().upsert(searchTerm)
        }
    }

    fun loadSuggestions(q: String) {
        scope.launch(Dispatchers.IO) {
            if (q.isBlank()) {
                _suggestedSearch.postValue(APIResponse.empty())
                _suggestedHistory.postValue(APIResponse.empty())
            } else {
                loadSuggestionFromDb(q).collect { _suggestedSearch.postValue(it) }
                loadSuggestedHistoryFromDb(q).collect { _suggestedHistory.postValue(it) }

            }
        }
    }
}