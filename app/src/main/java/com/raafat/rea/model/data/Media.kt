package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:28 AM.
 */
@Entity(
    tableName = "medias_table",
    foreignKeys = [
        ForeignKey(
            entity = Listing::class,
            parentColumns = ["listingId"],
            childColumns = ["listingId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Lister::class,
            parentColumns = ["listerId"],
            childColumns = ["listerId"]
        ),
        ForeignKey(
            entity = Organisation::class,
            parentColumns = ["organisationId"],
            childColumns = ["organisationId"]
        )
    ],
    indices = [
        Index("organisationId", unique = true),
        Index("listerId", unique = true),
        Index("id", unique = true),
        Index("listingId")
    ]
)
data class Media(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val type: String?,
    val url: String?,
    val thumbnailUrl: String?,
    val urlTemplate: String?,
    var listingId: String?,
    var listerId: Long?,
    var isCover: Boolean?,
    var organisationId: Long?

)