package com.raafat.rea.model.data

import androidx.room.*

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:04 AM.
 */
@Entity(
    tableName = "organisations_table",
    indices = [
        Index("organisationId",unique = true)
    ]
)
data class Organisation(
    @PrimaryKey
    @ColumnInfo(name = "organisationId")
    val id: Long,
    val type: String,
    val name: String
) {
    @Ignore
    var logo: Media? = null

    @Ignore
    var address: Address? = null

    @Ignore
    var contact: Contact? = null
}