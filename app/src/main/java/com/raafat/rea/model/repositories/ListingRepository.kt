package com.raafat.rea.model.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.raafat.rea.IPropertyApp
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.data.SearchTerm
import com.raafat.rea.model.repositories.db.IPropertyDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

/**
 * Created by Raafat Alhmidi on 3/14/2021 @6:55 AM.
 */
class ListingRepository(private val scope: CoroutineScope) {
    private val database = IPropertyDB.getDatabase(IPropertyApp.instance, scope)
    private val _listings = MutableLiveData<APIResponse<List<Listing>>>()
    private val _listingDetails = MutableLiveData<APIResponse<Listing?>>()

    val listingsLD: LiveData<APIResponse<List<Listing>>>
        get() = _listings
    val listingDetailsLD: LiveData<APIResponse<Listing?>>
        get() = _listingDetails

    private fun loadListingsFromDb(searchTerm: SearchTerm) = flow<APIResponse<List<Listing>>> {
        emit(APIResponse.loading())
        delay(1000)
        val list = database.listingDao().loadListing(
            searchTerm.title,
            searchTerm.getTypeInt(),
            searchTerm.addressLevel1 ?: "",
            searchTerm.addressLevel2 ?: ""
        ).map { it.getAsListing() }
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    private fun loadSavedFromDb() = flow<APIResponse<List<Listing>>> {
        emit(APIResponse.loading())
        delay(1000)
        val list = database.listingDao().loadSaved().map { it.getAsListing() }
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    private fun loadRecentVisitedFromDb() = flow<APIResponse<List<Listing>>> {
        emit(APIResponse.loading())
        delay(1000)
        val list = database.listingDao().loadRecentVisited().map { it.getAsListing() }
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    private fun loadDetailsFromDb(listingId: String) = flow<APIResponse<Listing?>> {
        emit(APIResponse.loading())
        delay(1000)
        val list = database.listingDao().loadDetails(listingId)?.getAsListing()
        if (list == null)
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }


    fun loadListingList(searchTerm: SearchTerm) {
        scope.launch(Dispatchers.IO) {
            loadListingsFromDb(searchTerm).collect { _listings.postValue(it) }
        }
    }

    fun updateListing(listing: Listing){
        scope.launch(Dispatchers.IO) {
            database.listingDao().update(listing)
        }
    }
    fun updateListing(listingId: String,time:Long){
        scope.launch(Dispatchers.IO) {
            database.listingDao().visit(listingId,time)
        }
    }

    fun loadSaved() {
        scope.launch(Dispatchers.IO) {
            loadSavedFromDb().collect { _listings.postValue(it) }
        }
    }

    fun loadRecentVisited() {
        scope.launch(Dispatchers.IO) {
            loadRecentVisitedFromDb().collect { _listings.postValue(it) }
        }
    }

    fun loadDetails(listingId: String) {
        scope.launch(Dispatchers.IO) {
            loadDetailsFromDb(listingId).collect { _listingDetails.postValue(it) }
        }
    }
}