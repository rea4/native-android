package com.raafat.rea.model.repositories.db

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.raafat.rea.IPropertyApp
import com.raafat.rea.getJsonDataFromAsset
import com.raafat.rea.model.data.*
import com.raafat.rea.model.repositories.APIResponse
import com.raafat.rea.model.repositories.db.dao.*
import com.raafat.rea.model.repositories.db.relations.ListingLister
import com.raafat.rea.model.repositories.db.relations.ListingOrganisation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject

/**
 * Created by Raafat Alhmidi on 3/9/2021 @5:39 PM.
 */
@Database(
    entities = [
        News::class, State::class, City::class, Address::class,
        Attribute::class, Contact::class, Media::class, Lister::class,
        Listing::class, Organisation::class, Phone::class, Email::class,
        Price::class, ListingLister::class, ListingOrganisation::class,
        SearchTerm::class
    ],
    version = 6
)
@TypeConverters(Convertors::class)
abstract class IPropertyDB : RoomDatabase() {


    abstract fun newsDao(): NewsDao
    abstract fun stateDao(): StatesDao
    abstract fun citiesDao(): CityDao
    abstract fun listingDao(): ListingDao
    abstract fun searchTermsDao(): SearchDao

    companion object {
        private var INSTANCE: IPropertyDB? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): IPropertyDB {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    IPropertyDB::class.java,
                    "iproperty_database.db"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(IPropertyDatabaseCallback(scope))
                    .build()

                INSTANCE = instance
                instance
            }
        }

        private class IPropertyDatabaseCallback(private val scope: CoroutineScope) :
            RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
            }

            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateNews(database.newsDao())
                        populateStates(database.stateDao())
                        populateListings(database.listingDao())
                    }

                }
            }

            private fun populateListings(listingDao: ListingDao) {
                listingDao.deleteAllListing()
                val response = populateListingsFromJson()
                response.data?.let { listingDao.insertAll(it) }
            }

            private fun populateStates(statesDao: StatesDao) {
                statesDao.deleteAll()
                statesDao.insertAll(populateStatesFromJson())
            }

            private fun populateNews(newsDao: NewsDao) {
                // Start the app with a clean database every time.
                // Not needed if you only populate on creation.
                newsDao.deleteAll()
                val list = populateNewsListFromJson("news.json").onEach {
                    it.newsType = NewsType.NEWS
                }
                newsDao.insert(list)
                newsDao.insert(populateNewsListFromJson("guides.json").onEach {
                    it.newsType = NewsType.GUIDES
                })
            }

            private fun populateNewsListFromJson(fileName: String): List<News> {
                val jsonData = IPropertyApp.instance.getJsonDataFromAsset(fileName)
                val gson = Gson()
                val resultType = object : TypeToken<List<News>>() {}.type
                return gson.fromJson(jsonData, resultType)
            }

            private fun populateListingsFromJson(): APIResponse<List<Listing>> {
                val jsonData =
                    IPropertyApp.instance.getJsonDataFromAsset("search_results_page.json")

                val gson = Gson()
                val resultType = object : TypeToken<APIResponse<List<Listing>>>() {}.type
                return gson.fromJson(jsonData, resultType)

            }

            private fun populateStatesFromJson(): List<State> {
                val jsonData = IPropertyApp.instance.getJsonDataFromAsset("states_cities.json")
                val jsonObject = JSONObject(jsonData)
                val states = mutableListOf<State>()
                jsonObject.keys().forEach {
                    val state = State(name = it)
                    val cities = mutableListOf<City>()
                    val citiesJA = jsonObject.getJSONArray(it)
                    for (idx in 0 until citiesJA.length()) {
                        cities.add(City(name = citiesJA[idx].toString(), stateName = it))
                    }
                    states.add(state.apply { this.cities = cities })
                }
                return states
            }
        }

    }
}