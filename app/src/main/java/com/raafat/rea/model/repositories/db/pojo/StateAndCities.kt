package com.raafat.rea.model.repositories.db.pojo

import androidx.room.Embedded
import androidx.room.Relation
import com.raafat.rea.model.data.City
import com.raafat.rea.model.data.State

/**
 * Created by Raafat Alhmidi on 3/12/2021 @11:52 AM.
 */
data class StateAndCities(
    @Embedded val state: State,
    @Relation(
        parentColumn = "name",
        entityColumn = "stateName"
    )
    val cities: MutableList<City>
) {
    fun getAsState(): State {
        state.cities = cities
        return state
    }
}