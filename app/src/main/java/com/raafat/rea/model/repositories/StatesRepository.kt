package com.raafat.rea.model.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.raafat.rea.IPropertyApp
import com.raafat.rea.model.data.City
import com.raafat.rea.model.data.State
import com.raafat.rea.model.repositories.db.IPropertyDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

/**
 * Created by Raafat Alhmidi on 3/12/2021 @12:03 PM.
 */
class StatesRepository(private val scope: CoroutineScope) {
    private val database = IPropertyDB.getDatabase(IPropertyApp.instance, scope)
    private val _states = MutableLiveData<APIResponse<List<State>>>()
    private val _cities = MutableLiveData<APIResponse<List<City>>>()
    private val _state = MutableLiveData<APIResponse<State>>()

    val statesLD: LiveData<APIResponse<List<State>>>
        get() = _states
    val citiesLD: LiveData<APIResponse<List<City>>>
        get() = _cities
    val stateLD: LiveData<APIResponse<State>>
        get() = _state

    private fun loadStatesFromDb(q: String) = flow<APIResponse<List<State>>> {
        emit(APIResponse.loading())
        val list = database.stateDao().loadAll(q).map { it.getAsState() }
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    private fun loadCitiesFromDb(state: String, q: String) = flow<APIResponse<List<City>>> {
        emit(APIResponse.loading())
        val list = database.stateDao().loadAllCitiesForState(state, q)
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    fun updateState(state: State){
        scope.launch(Dispatchers.IO){
            database.stateDao().update(state)
        }
    }
    fun updateCity(city: City){
        scope.launch(Dispatchers.IO){
            database.citiesDao().update(city)
        }
    }

    fun loadStates(q: String) {
        scope.launch(Dispatchers.IO) {
            loadStatesFromDb(q).collect { _states.postValue(it) }
        }
    }

    fun loadCities(state: String, q: String) {
        scope.launch(Dispatchers.IO) {
            loadCitiesFromDb(state, q).collect { _cities.postValue(it) }
        }
    }

}