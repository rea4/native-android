package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by Raafat Alhmidi on 3/9/2021 @12:51 AM.
 */
@Entity(
    tableName = "news_table",
    indices = [
        Index("id",unique = true)
    ]
)
data class News(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @SerializedName("title")
    val title: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("img_url")
    val imgUrl: String,
    var newsType: NewsType
)

enum class NewsType {
    GUIDES, NEWS, LIFESTYLE
}