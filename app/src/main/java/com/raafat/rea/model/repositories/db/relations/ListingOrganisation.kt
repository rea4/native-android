package com.raafat.rea.model.repositories.db.relations

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import com.raafat.rea.model.data.Listing
import com.raafat.rea.model.data.Organisation

/**
 * Created by Raafat Alhmidi on 3/11/2021 @10:34 PM.
 * This entity to represent many-to-many relationship between listing and organisation
 */

@Entity(
    tableName = "listing_organisation_table",
    primaryKeys = ["organisationId", "listingId"],
    foreignKeys = [
        ForeignKey(
            entity = Listing::class,
            parentColumns = ["listingId"],
            childColumns = ["listingId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Organisation::class,
            parentColumns = ["organisationId"],
            childColumns = ["organisationId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("organisationId"),
        Index("listingId")
    ]
)
data class ListingOrganisation(
    val organisationId: Long,
    val listingId: String
)