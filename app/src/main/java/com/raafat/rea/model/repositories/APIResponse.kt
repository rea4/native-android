package com.raafat.rea.model.repositories

import com.google.gson.annotations.SerializedName

/**
 * Created by Raafat Alhmidi on 3/9/2021 @6:36 PM.
 */
class APIResponse<T>(
    @SerializedName("items")
    val data: T? = null,
    val message: String? = null,
    private val status: Status = Status.LOADING
) {

    val isLoading: Boolean
        get() {
            return status == Status.LOADING
        }
    val isError: Boolean
        get() {
            return status == Status.ERROR
        }
    val isSuccess: Boolean
        get() {
            return status == Status.SUCESS
        }
    val isEmpty: Boolean
        get() {
            return status == Status.EMPTY
        }

    companion object {
        fun <T> success(data: T): APIResponse<T> {
            return APIResponse(data = data, status = Status.SUCESS)
        }

        fun <T> error(msg: String?, data: T?): APIResponse<T?> {
            return APIResponse(data = data, message = msg, status = Status.ERROR)
        }

        fun <T> empty(): APIResponse<T> {
            return APIResponse(status = Status.EMPTY)
        }

        fun <T> loading(): APIResponse<T> {
            return APIResponse(status = Status.LOADING)
        }
    }
}

enum class Status {
    LOADING, ERROR, EMPTY, SUCESS
}