package com.raafat.rea.model.repositories.db.relations

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import com.raafat.rea.model.data.Lister
import com.raafat.rea.model.data.Listing

/**
 * Created by Raafat Alhmidi on 3/11/2021 @10:34 PM.
 * This entity to represent many-to-many relationship between listing and lister
 */

@Entity(
    tableName = "listing_listers_table",
    primaryKeys = ["listerId", "listingId"],
    foreignKeys = [
        ForeignKey(
            entity = Listing::class,
            parentColumns = ["listingId"],
            childColumns = ["listingId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Lister::class,
            parentColumns = ["listerId"],
            childColumns = ["listerId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("listerId"),
        Index("listingId")
    ]
)
data class ListingLister(
    val listerId: Long,
    val listingId: String
)