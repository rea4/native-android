package com.raafat.rea.model.repositories.db.pojo

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.raafat.rea.model.data.*
import com.raafat.rea.model.repositories.db.relations.ListingLister
import com.raafat.rea.model.repositories.db.relations.ListingOrganisation

/**
 * Created by Raafat Alhmidi on 3/13/2021 @2:02 PM.
 */
data class ListingDetails(
    @Embedded
    val listing: Listing,
    @Relation(
        parentColumn = "listingId",
        entityColumn = "listingId"
    )
    val atribute: Attribute?,
    @Relation(
        parentColumn = "listingId",
        entityColumn = "listingId"
    )
    val address: Address?,
    @Relation(
        parentColumn = "listingId",
        entityColumn = "listerId",
        associateBy = Junction(ListingLister::class),
        entity = Lister::class
    )
    val listers: List<ListerDetails>?,
    @Relation(
        parentColumn = "listingId",
        entityColumn = "organisationId",
        associateBy = Junction(ListingOrganisation::class),
        entity = Organisation::class
    )
    val organisations: List<OrganisationDetails>,
    @Relation(
        parentColumn = "listingId",
        entityColumn = "listingId"
    )
    val prices:List<Price>,
    @Relation(
        parentColumn = "listingId",
        entityColumn = "listingId"
    )
    val media:List<Media>
) {
    fun getAsListing(): Listing {
        listing.attributes = atribute
        listing.listers = listers?.mapNotNull { it.getAsLister() }
        listing.organisations = organisations.map { it.getAsOrganisation() }
        listing.address = address
        listing.prices = prices
        listing.medias = media
        listing.cover = media.firstOrNull { it.isCover == true }
        (if(listing.listers?.size == 0) null else listing.listers).also { listing.listers = it }
        return listing
    }
}