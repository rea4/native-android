package com.raafat.rea.model.repositories.db.dao

import androidx.room.*

/**
 * Created by Raafat Alhmidi on 3/9/2021 @5:45 PM.
 */
@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: T):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: List<T>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertOrIgnore(item: T):Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertOrIgnore(item: List<T>):List<Long>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(item: T)


}