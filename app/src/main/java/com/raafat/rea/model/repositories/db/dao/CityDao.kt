package com.raafat.rea.model.repositories.db.dao

import androidx.room.Dao
import com.raafat.rea.model.data.City

/**
 * Created by Raafat Alhmidi on 3/9/2021 @10:58 PM.
 */
@Dao
abstract class CityDao : BaseDao<City> {

}