package com.raafat.rea.model.data

import androidx.room.*
import java.text.DecimalFormat

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:09 AM.
 */
@Entity(
    tableName = "attributes_table",
    foreignKeys = [
        ForeignKey(
            entity = Listing::class,
            parentColumns = ["listingId"],
            childColumns = ["listingId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("listingId", unique = true),
        Index("id", unique = true)
    ]
)
data class Attribute(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val landArea: String?,
    val bathroom: String?,
    val bedroom: String?,
    val carPark: String?,
    val builtUp: String?,
    val landTitleType: String?,
    val furnishing: String?,
    val tenure: String?,
    val unitType: String?,
    val sizeUnit: String?,
    val completionDate: String?,
    val pricePSF: String?,
    val maximumPricePerSizeUnit: String?,
    val minimumPricePerSizeUnit: String?,
    val highlight: String?,
    var listingId: String?
) {
    @Ignore
    private val formatter = DecimalFormat("#,###,###")
    fun getSizeUnitFormated() = when (pricePSF) {
        "SQUARE_FEET" -> "sq. ft."
        else -> "sq. ft"
    }

    fun getPricePSFFormated() =
        if (pricePSF?.apply { this.filterNot { it == ',' } }?.toIntOrNull() == null)
            if(pricePSF.isNullOrBlank()) "" else " (RM $pricePSF/${getSizeUnitFormated()})"
        else
            " (RM ${formatter.format(pricePSF.toInt())}/${getSizeUnitFormated()})"

    private fun getLandAreaFormated(): String =
        if (landArea?.apply { this.filterNot { it == ',' } }?.toIntOrNull() == null)
            if (landArea.isNullOrBlank()) "" else "Land Area: $landArea\n"
        else
            "Land Area: ${formatter.format(landArea.toInt()) + getSizeUnitFormated()}"+" "+"${getPricePSFFormated()}\n"


    fun getBuildUpFormated() =
        if (builtUp?.apply { this.filterNot { it == ',' } }?.toIntOrNull() == null)
            if(builtUp.isNullOrBlank())"" else "Built-up Size: $builtUp ${getSizeUnitFormated()} ${getPricePSFFormated()}\n"
        else
            "Built-up Size: ${formatter.format(builtUp.toInt())} ${getSizeUnitFormated()}  ${getPricePSFFormated()}\n"

    fun getFurnishingFormated() =
        if (furnishing.isNullOrBlank())
            ""
        else
            "Furnishing: $furnishing\n"

    fun getFormatedValue() = getBuildUpFormated() + getLandAreaFormated() + getFurnishingFormated()

}