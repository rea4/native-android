package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Created by Raafat Alhmidi on 3/11/2021 @11:30 AM.
 */
@Entity(
    tableName = "emails_table",
    foreignKeys = [
        ForeignKey(
            entity = Contact::class,
            parentColumns = ["id"],
            childColumns = ["contactId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("contactId"),
        Index("id", unique = true)
    ]
)
data class Email(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val email: String,
    val contactId: Long

)