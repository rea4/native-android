package com.raafat.rea.model.repositories.db.pojo

import androidx.room.Embedded
import androidx.room.Relation
import com.raafat.rea.model.data.Contact
import com.raafat.rea.model.data.Email
import com.raafat.rea.model.data.Phone

/**
 * Created by Raafat Alhmidi on 3/13/2021 @10:59 PM.
 */
data class ContactDetails(
    @Embedded
    val contact: Contact,
    @Relation(
        parentColumn = "id",
        entityColumn = "contactId"
    )
    val phones:List<Phone>,
    @Relation(
        parentColumn = "id",
        entityColumn = "contactId"
    )
    val emails:List<Email>
){
    fun getAsContact():Contact{
        contact.phones = phones
        contact.emails = emails.map { it.email }
        return contact
    }
}