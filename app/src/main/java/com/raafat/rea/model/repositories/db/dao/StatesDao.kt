package com.raafat.rea.model.repositories.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.raafat.rea.model.data.City
import com.raafat.rea.model.data.State
import com.raafat.rea.model.repositories.db.pojo.StateAndCities

/**
 * Created by Raafat Alhmidi on 3/9/2021 @10:58 PM.
 */
@Dao
abstract class StatesDao : BaseDao<State> {

    @Query("DELETE FROM states_table")
    abstract fun deleteAll()

    @Transaction
    @Query("SELECT * FROM states_table WHERE states_table.name LIKE '%' || :q || '%'")
    abstract fun loadAll(q:String): List<StateAndCities>

    @Query("SELECT * from cities_table WHERE (name LIKE '%' || :q || '%') AND (stateName = :state OR stateName IS NULL) ORDER BY stateName ASC , name ASC ")
    abstract fun loadAllCitiesForState(state: String,q:String): List<City>

    @Insert
    abstract fun insertCities(items: List<City>)

    fun insertAll(items: List<State>) {
        insert(items)
        var cities = items.onEach {
            it.cities = listOf(City(name = "Any",stateName = null)) + it.cities
            it.cities.onEach { city -> city.stateName = it.name }
        }.flatMap { it.cities }
        insertCities(cities)
    }


}