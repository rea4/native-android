package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import com.raafat.rea.utils.calculateMortage
import java.text.DecimalFormat

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:29 AM.
 */
@Entity(
    tableName = "prices_table",
    foreignKeys = [
        ForeignKey(
            entity = Listing::class,
            parentColumns = ["listingId"],
            childColumns = ["listingId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("listingId"),
        Index("id", unique = true)
    ]
)
data class Price(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    val type: String,
    val currency: String,
    val max: Int,
    val min: Int,
    var listingId: String?

) {
    fun getFormatedPrice(): String {
        val decimalFormat = DecimalFormat("#,###,###")
        return if (max != 0)
            "RM " + decimalFormat.format(max)
        else if (min != 0)
            "RM " + decimalFormat.format(min)
        else
            ""
    }

    fun getMortgage(): String {
        val decimalFormat = DecimalFormat("#,###,###")
        return if (max != 0)
            "RM " + decimalFormat.format(calculateMortage(max))
        else if (min != 0)
            "RM " + decimalFormat.format(calculateMortage(min))
        else
            ""

    }
}
