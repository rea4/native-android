package com.raafat.rea.model.repositories.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.raafat.rea.model.data.SearchTerm

/**
 * Created by Raafat Alhmidi on 3/11/2021 @10:49 PM.
 */
@Dao
abstract class SearchDao : BaseDao<SearchTerm> {

    @Query("SELECT * FROM search_terms_table WHERE title = :title AND type = :type AND addressLevel1 = :addressL1")
    abstract fun loadSearch(title: String, type: String,addressL1:String): SearchTerm?

    @Query("SELECT * FROM search_terms_table ORDER BY searchDate DESC")
    abstract fun loadHistory(): List<SearchTerm>

    @Query("SELECT * FROM search_terms_table WHERE title LIKE '%' || :q || '%' ORDER BY searchDate DESC ")
    abstract fun loadSuggestedHistory(q: String): List<SearchTerm>

    @Query("SELECT * FROM (SELECT a.title , a.propertyType as type ,b.level1 AS addressLevel1,b.level2 AS addressLevel2,'ic_round_location_city_24' as icon FROM listings_table AS a INNER JOIN addresses_table AS b ON a.listingId = b.listingId WHERE a.title LIKE '%'||:q||'%'  )  UNION ALL SELECT * FROM (SELECT states_table.name as title, 'State' as type ,NULL as addressLevel1, NULL addressLevel2 , 'ic_twotone_location_on_24' AS icon FROM states_table WHERE states_table.name LIKE '%'||:q||'%' ) UNION ALL SELECT * FROM (SELECT b.name as title, 'City/Area' AS type, a.name AS addressLevel1, NULL AS addressLevel2 , 'ic_twotone_location_on_24' AS icon FROM states_table as a INNER JOIN cities_table as b ON a.name = b.stateName WHERE b.name LIKE '%'||:q||'%' AND b.name <> 'Any' ) ")
    abstract fun loadSuggestions(q: String): List<SearchTerm>

    fun upsert(searchTerm: SearchTerm) {
        searchTerm.searchDate = System.currentTimeMillis()
        val _searchTerm = loadSearch(searchTerm.title,searchTerm.type,searchTerm.addressLevel1?:"")
        searchTerm.id = _searchTerm?.id
        val id = insert(searchTerm)
        searchTerm.id = id


    }


}