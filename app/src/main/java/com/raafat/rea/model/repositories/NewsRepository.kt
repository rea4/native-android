package com.raafat.rea.model.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.raafat.rea.IPropertyApp
import com.raafat.rea.model.data.News
import com.raafat.rea.model.data.NewsType
import com.raafat.rea.model.repositories.db.IPropertyDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

/**
 * Created by Raafat Alhmidi on 3/9/2021 @6:16 PM.
 */
class NewsRepository(private val scope: CoroutineScope) {
    private val database = IPropertyDB.getDatabase(IPropertyApp.instance, scope)
    private val _newsLiveData: MutableLiveData<APIResponse<List<News>>> = MutableLiveData()
    private val _guidesLiveData: MutableLiveData<APIResponse<List<News>>> = MutableLiveData()
    private val _lifestyleLiveData: MutableLiveData<APIResponse<List<News>>> = MutableLiveData()

    val newsLiveData: LiveData<APIResponse<List<News>>>
        get() = _newsLiveData

    val guidesLiveData: LiveData<APIResponse<List<News>>>
        get() = _guidesLiveData
    val lifestyleLiveData: LiveData<APIResponse<List<News>>>
        get() = _lifestyleLiveData


    private fun simulateLoadingNews(newsType: NewsType) = flow<APIResponse<List<News>>> {
        emit(APIResponse.loading())
        delay(1000)
        val list = database.newsDao().loadAllNews(newsType)
        if (list.isNullOrEmpty())
            emit(APIResponse.empty())
        else
            emit(APIResponse.success(list))
    }

    fun loadNews() {
        scope.launch(Dispatchers.IO) {
            simulateLoadingNews(NewsType.NEWS).collect {
                _newsLiveData.postValue(it)
            }
            simulateLoadingNews(NewsType.GUIDES).collect {
                _guidesLiveData.postValue(it)
            }
            simulateLoadingNews(NewsType.LIFESTYLE).collect {
                _lifestyleLiveData.postValue(it)
            }
        }
    }

}