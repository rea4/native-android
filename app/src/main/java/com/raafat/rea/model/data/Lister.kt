package com.raafat.rea.model.data

import androidx.room.*

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:05 AM.
 */
@Entity(
    tableName = "listers_table",
    indices = [
        Index("listerId", unique = true)
    ]
)
data class Lister(
    @PrimaryKey
    @ColumnInfo(name = "listerId")
    val id: Long,
    val type: String?,
    val name: String,
    val license: String?,
    val website: String?

) {
    @Ignore
    var image: Media? = null

    @Ignore
    var contact: Contact? = null
}