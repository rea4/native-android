package com.raafat.rea.model.repositories.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.raafat.rea.model.data.News
import com.raafat.rea.model.data.NewsType

/**
 * Created by Raafat Alhmidi on 3/9/2021 @5:46 PM.
 */
@Dao
interface NewsDao : BaseDao<News> {

    @Query("DELETE FROM news_table")
    fun deleteAll()

    @Query("SELECT * FROM news_table WHERE newsType =:newsType ")
    fun loadAllNews(newsType: NewsType): List<News>
}