package com.raafat.rea.model.repositories.db.pojo

import androidx.room.Embedded
import androidx.room.Relation
import com.raafat.rea.model.data.Contact
import com.raafat.rea.model.data.Media
import com.raafat.rea.model.data.Lister

/**
 * Created by Raafat Alhmidi on 3/13/2021 @6:54 PM.
 */

data class ListerDetails(
    @Embedded
    val lister: Lister?,
    @Relation(
        parentColumn = "listerId",
        entityColumn = "listerId",
        entity = Contact::class
    )
    val contact: ContactDetails?,
    @Relation(
        parentColumn = "listerId",
        entityColumn = "listerId"
    )
    val media: Media?
){
    fun getAsLister():Lister?{
        lister?.contact = contact?.getAsContact()
        lister?.image = media
        return lister
    }
}
