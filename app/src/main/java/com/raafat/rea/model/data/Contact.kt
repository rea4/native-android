package com.raafat.rea.model.data

import androidx.room.*

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:28 AM.
 */
@Entity(
    tableName = "contacts_table",
    foreignKeys = [

        ForeignKey(
            entity = Lister::class,
            parentColumns = ["listerId"],
            childColumns = ["listerId"]
        ),
        ForeignKey(
            entity = Organisation::class,
            parentColumns = ["organisationId"],
            childColumns = ["organisationId"]
        )
    ],
    indices = [
        Index("organisationId", unique = true),
        Index("id", unique = true),
        Index("listerId",unique = true)
    ]
)
data class Contact(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val listerId: Long?,
    var organisationId: Long?
) {
    @Ignore
    var phones: List<Phone>? = null

    @Ignore
    var emails: List<String>? = null
}