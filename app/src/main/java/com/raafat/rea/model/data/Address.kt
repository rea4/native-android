package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:29 AM.
 */
@Entity(
    tableName = "addresses_table",
    foreignKeys = [
        ForeignKey(
            entity = Listing::class,
            parentColumns = ["listingId"],
            childColumns = ["listingId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Organisation::class,
            parentColumns = ["organisationId"],
            childColumns = ["organisationId"]
        )
    ],
    indices = [
        Index("organisationId", unique = true),
        Index("listingId", unique = true),
        Index("id", unique = true)
    ]
)
data class Address(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val formattedAddress: String,
    val lat: Double?,
    val lng: Double?,
    var level1: String?,
    var level2: String?,
    var level3: String?,
    var listingId: String?,
    var organisationId: Long?
) {
    fun getAddressString(): String {
        return if (formattedAddress.isBlank()) "$level2 , $level1 "
        else formattedAddress
    }
}