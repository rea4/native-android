package com.raafat.rea.model.repositories.db

import androidx.room.TypeConverter
import com.raafat.rea.model.data.NewsType
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Raafat Alhmidi on 3/9/2021 @6:04 PM.
 */
class Convertors {
//    private val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)

    @TypeConverter
    fun toNewsType(value: Int) = enumValues<NewsType>()[value]

    @TypeConverter
    fun fromNewsType(value: NewsType) = value.ordinal

//    @TypeConverter
//    fun fromTimeStamp(value: Long): String = sdf.format(value)


}