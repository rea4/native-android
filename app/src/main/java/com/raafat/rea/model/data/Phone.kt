package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:28 AM.
 */
@Entity(
    tableName = "phones_table",
    foreignKeys = [
        ForeignKey(
            entity = Contact::class,
            parentColumns = ["id"],
            childColumns = ["contactId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("contactId"),
        Index("id", unique = true)
    ]
)
data class Phone(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val label: String,
    val number: String,
    var contactId: Long

)