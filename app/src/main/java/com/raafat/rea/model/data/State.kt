package com.raafat.rea.model.data

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

/**
 * Created by Raafat Alhmidi on 3/9/2021 @10:53 PM.
 */
@Entity(
    tableName = "states_table",
    indices = [
        Index("name", unique = true)
    ]
)
data class State(
    @PrimaryKey
    val name: String,
    var selected: Boolean = false
):Serializable {
    @Ignore
    var cities: List<City> = listOf()

}