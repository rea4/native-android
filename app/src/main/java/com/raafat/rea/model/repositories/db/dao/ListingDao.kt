package com.raafat.rea.model.repositories.db.dao

import androidx.room.*
import com.raafat.rea.model.data.*
import com.raafat.rea.model.repositories.db.pojo.ListingDetails
import com.raafat.rea.model.repositories.db.relations.ListingLister
import com.raafat.rea.model.repositories.db.relations.ListingOrganisation

/**
 * Created by Raafat Alhmidi on 3/11/2021 @2:16 AM.
 */
//SearchTerm(id=null, title=Taman Tun Dr Ismail, type=City/Area, addressLevel1=Kuala Lumpur, addressLevel2=null, icon=ic_twotone_location_on_24, searchDate=1615672882190)
@Dao
abstract class ListingDao : BaseDao<Listing> {

    @Transaction
    @Query("SELECT a.* FROM listings_table AS a INNER JOIN addresses_table AS b ON a.listingId = b.listingId WHERE ( a.title LIKE '%' || :title || '%' AND :type = 3) OR (b.level1 LIKE '%' || :title || '%' AND :type = 1) OR (b.level2 LIKE '%' || :title || '%' AND b.level1 LIKE '%' || :state || '%' AND :type = 2 ) OR ( b.level1 LIKE '%' || :state || '%' AND b.level2 LIKE '%' || :city || '%' AND a.title LIKE '%' || :title || '%' AND :type = 4) ")
    abstract fun loadListing(
        title: String,
        type: Int,
        state: String,
        city: String
    ): List<ListingDetails>

    @Transaction
    @Query("SELECT * FROM listings_table WHERE saved = 1 ")
    abstract fun loadSaved(): List<ListingDetails>

    @Query("UPDATE listings_table SET visitDate = :time WHERE listingId = :listingId")
    abstract fun visit(listingId: String, time: Long)


    @Transaction
    @Query("SELECT * FROM listings_table WHERE visitDate <> 0 ORDER BY visitDate DESC ")
    abstract fun loadRecentVisited(): List<ListingDetails>

    @Transaction
    @Query("SELECT * FROM listings_table WHERE listingId = :listingId")
    abstract fun loadDetails(listingId: String): ListingDetails?

    @Query("DELETE FROM listings_table")
    abstract fun deleteAllListing()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAddress(address: Address)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertAddress(addresses: List<Address>)

    @Insert
    abstract fun insertAttribute(attribute: Attribute)

    @Insert
    abstract fun insertAttribute(attributes: List<Attribute>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertImages(media: List<Media>)

    @Insert
    abstract fun insertPrices(prices: List<Price>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertListers(listers: List<Lister>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertListingListers(listingListers: List<ListingLister>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertOrganisations(organisations: List<Organisation>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertListingOrganisation(listingOrganisation: List<ListingOrganisation>)

    @Insert
    abstract fun insertContacts(contacts: List<Contact>): List<Long>

    @Insert
    abstract fun insertEmails(emails: List<Email>)

    @Insert
    abstract fun insertPhones(phones: List<Phone>)

    private fun insertContactWithDetails(contacts: List<Contact>) {
        val ids = insertContacts(contacts)
        val emails = mutableListOf<Email>()
        var phones = listOf<Phone>()
        contacts.onEachIndexed { index, contact ->
            val id = ids[index]
            contact.emails?.mapTo(emails) { Email(null, it, id) }
            contact.phones?.onEach { it.contactId = id }?.apply {
                phones = phones + this
            }

        }
        insertPhones(phones)
        insertEmails(emails)
    }

    fun insertAll(listings: List<Listing>) {
        insert(listings)

        //prepare foreign keys
        listings.onEach {
            it.prices?.onEach { price -> price.listingId = it.id }
            it.cover?.let { image ->
                image.listingId = it.id
                image.isCover = true
            }
            it.medias?.let { medias ->
                medias.onEach { image -> image.listingId = it.id }
                it.cover?.let { cover ->
                    it.medias = medias + cover
                }

            }
            it.multilanguagePlace?.let { multilanguagePlace ->

                it.address = Address(
                    it.address?.id,
                    it.address?.formattedAddress ?: "",
                    it.address?.lat,
                    it.address?.lng,
                    multilanguagePlace.enGB.level1,
                    multilanguagePlace.enGB.level2,
                    multilanguagePlace.enGB.level3,
                    it.address?.listingId,
                    it.address?.organisationId
                )

            }
            it.address?.let { address -> address.listingId = it.id }
            it.attributes?.let { attribute -> attribute.listingId = it.id }
            it.listers?.onEach { lister ->
                lister.image?.listerId = lister.id

            }
            it.organisations?.onEach { organisation ->
                organisation.address?.organisationId = organisation.id
                organisation.contact?.organisationId = organisation.id
                organisation.logo?.organisationId = organisation.id
            }
        }

        //insert prices
        listings.flatMap { it.prices ?: listOf() }.apply { insertPrices(this) }
        //insert images and cover
        var images = listings.flatMap { it.medias ?: listOf() }
        //insert prices
        listings.flatMap { it.prices ?: listOf() }.apply { insertPrices(this) }
        //insert address
        var addresses = listings.mapNotNull { it.address }
        //insert attributes
        listings.mapNotNull { it.attributes }.apply { insertAttribute(this) }
        //insert listers
        val listers = listings.flatMap { it.listers ?: listOf() }.apply {
            insertListers(this)
        }
        //insert listingListers
        listings.flatMap {
            it.listers?.map { lister -> ListingLister(lister.id, it.id) } ?: listOf()
        }.apply { insertListingListers(this) }

        //insert organisations
        val organinsations = listings.flatMap { it.organisations ?: listOf() }.apply {
            insertOrganisations(this)
        }
        //insert listingOrganisations
        listings.flatMap {
            it.organisations?.map { organisation ->
                ListingOrganisation(
                    organisation.id,
                    it.id
                )
            } ?: listOf()
        }.apply { insertListingOrganisation(this) }


        val contacts = organinsations.mapNotNull { it.contact } + listers.mapNotNull { it.contact }
        images = images + organinsations.mapNotNull { it.logo } + listers.mapNotNull { it.image }
        addresses = addresses + organinsations.mapNotNull { it.address }
        //insert contacts for organisation and lister
        insertContactWithDetails(contacts)
        //insert images of listings, listers and organisations
        insertImages(images)
        //insert address of listings and organinsations
        insertAddress(addresses)

    }


}