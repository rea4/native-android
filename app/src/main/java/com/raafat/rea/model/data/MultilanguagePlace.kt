package com.raafat.rea.model.data

import com.google.gson.annotations.SerializedName

/**
 * Created by Raafat Alhmidi on 3/10/2021 @12:29 AM.
 */
data class MultilanguagePlace(
    @SerializedName("en-GB")
    val enGB: Address
)