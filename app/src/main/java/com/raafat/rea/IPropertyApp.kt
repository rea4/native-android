package com.raafat.rea

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate

/**
 * Created by Raafat Alhmidi on 3/7/2021 @1:52 PM.
 */
class IPropertyApp : Application() {

    companion object{
        lateinit var instance:IPropertyApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

    }
}