package com.raafat.rea

import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.raafat.rea.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        val navController = findNavController(R.id.nav_host_fragment)
        binding.navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_home -> {
                    binding.navView.visibility = View.VISIBLE
                    changeStatusBarColor(R.color.primary_light)
                    changeStatusBarIconColor(false)
                }
                else -> {
                    if(destination.id == R.id.searchFragment)
                        binding.navView.visibility = View.GONE
                    changeStatusBarColor(R.color.white)
                    changeStatusBarIconColor(true)
                }
            }
        }
    }
}