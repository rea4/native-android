package com.raafat.rea.utils

import android.graphics.drawable.Drawable
import android.text.Html
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.raafat.rea.IPropertyApp
import com.raafat.rea.R
import com.raafat.rea.model.data.*
import com.raafat.rea.ui.common.MediasAdapter
import com.raafat.rea.ui.common.NewsAdapter
import com.raafat.rea.ui.common.ListingAdapter
import com.raafat.rea.ui.search.SearchTermsAdapter
import com.raafat.rea.ui.stateSelection.CitiesAdapter
import com.raafat.rea.ui.stateSelection.StatesAdapter


/**
 * Created by Raafat Alhmidi on 3/9/2021 @1:05 PM.
 */
@BindingAdapter(value = ["imageUrl", "circular"], requireAll = false)
fun loadImage(view: ImageView, imageUrl: String?, circular: Boolean = false) {
    if (!TextUtils.isEmpty(imageUrl)) {
        val glide = Glide.with(IPropertyApp.instance)
            .load(imageUrl)
            .placeholder(R.drawable.ic_baseline_image_24)

        if (circular)
            glide.circleCrop()
        glide.into(view)
    } else
        view.setImageResource(0)
}

@BindingAdapter("randomColor")
fun randomColor(view: View, value: Boolean = false) {
    if (value) {
        view.setBackgroundColor(generateRandomColor())
    }

}

@BindingAdapter("android:src")
fun loadDrawable(view: ImageView, drawableId: String?) {
    val id: Int =
        view.context.resources.getIdentifier(drawableId, "drawable", view.context.packageName)
    view.setImageResource(id)

}

@BindingAdapter("news", "listener", requireAll = false)
fun setNews(rv: RecyclerView, items: List<News>?, listener: ItemClickListener?) {
    val adapter = NewsAdapter(listener)
    if (items != null) {
        adapter.data = items
    }
    rv.adapter = adapter
}

@BindingAdapter("listings", "listener", "itemLayout", requireAll = false)
fun setListings(
    rv: RecyclerView,
    items: List<Listing>?,
    listener: ItemClickListener?,
    itemLayout: Int
) {
    val adapter = ListingAdapter(listener, itemLayout)
    if (items != null) {
        adapter.data = items
    }
    rv.adapter = adapter
}

@BindingAdapter("states", "listener")
fun setStates(rv: RecyclerView, items: List<State>?, listener: ItemClickListener?) {
    val adapter = StatesAdapter(listener)
    if (items != null) {
        adapter.data = items
    }
    rv.adapter = adapter
    val dividerItemDecoration = DividerItemDecoration(
        rv.context,
        LinearLayoutManager.VERTICAL
    )

    rv.addItemDecoration(dividerItemDecoration)
}

@BindingAdapter("cities", "listener")
fun setCities(
    rv: RecyclerView, items: List<City>?, listener: ItemClickListener?
) {
    val adapter = CitiesAdapter(listener)
    if (items != null) {
        adapter.data = items
    }
    rv.adapter = adapter
    val dividerItemDecoration = DividerItemDecoration(
        rv.context,
        LinearLayoutManager.VERTICAL
    )

    rv.addItemDecoration(dividerItemDecoration)
}

@BindingAdapter("suggestions", "listener", "query", requireAll = false)
fun setSuggestions(
    rv: RecyclerView,
    items: List<SearchTerm>?,
    listener: ItemClickListener?,
    query: String?
) {
    val adapter = SearchTermsAdapter(listener, query)
    if (items != null) {
        adapter.data = items
    }
    rv.adapter = adapter
    val dividerItemDecoration = DividerItemDecoration(
        rv.context,
        LinearLayoutManager.VERTICAL
    )

    rv.addItemDecoration(dividerItemDecoration)


}

@BindingAdapter("medias", "listener", "itemLayout", "positionTxtV", requireAll = false)
fun setMedias(
    vp: ViewPager2,
    items: List<Media>?,
    listener: ItemClickListener?,
    itemLayout: Int,
    positionTxtV: TextView? = null
) {
    val adapter = MediasAdapter(listener, itemLayout)
    if (items != null) {
        adapter.data = items
    }
    vp.adapter = adapter

    positionTxtV?.let {
        it.text = "1/${items?.size ?: 0}"

        vp.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                it.text = "${position + 1}/${items?.size ?: 0}"
            }
        })
    }
}

@BindingAdapter("android:text")
fun EditText.bindString(value: String?) {
    value?.let {

        setText(value)
        tag = value
        setSelection(value.length)


    }
}

@BindingAdapter("app:text")
fun TextView.bindString(value: String?) {
    text = ""
    value?.let {
        text = Html.fromHtml(value)
    }
}

@BindingAdapter("app:savedBtn")
fun CustomToolBar.setFirstBtnDrawable(saved: Boolean) {

    this.setFirstBtnIcon(
        if (saved) R.drawable.ic_baseline_star_24_yellow else R.drawable.ic_baseline_star_outline_24
    )

}
