package com.raafat.rea.utils

import android.graphics.Color
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.random.Random

/**
 * Created by Raafat Alhmidi on 3/10/2021 @1:31 AM.
 */
fun calculateMortage(
    totlaValue: Int,
    downPayment: Int = (totlaValue * 0.1).roundToInt(),
    annualIR: Double = 4.7,
    loanTerm: Int = 35
): Int {
    val r = annualIR / 100 / 12
    val p = totlaValue - downPayment
    val n = loanTerm * 12

    return when {
        r != 0.0 -> ((r * p * (1 + r).pow(n)) / ((1 + r).pow(n) - 1)).roundToInt()
        n != 0 -> (p.toDouble() / n).roundToInt()
        else -> 0
    }
}

fun generateRandomColor(): Int {
     val  mRandom = Random(System.currentTimeMillis());

    val baseColor: Int = Color.WHITE
    val baseRed: Int = Color.red(baseColor)
    val baseGreen: Int = Color.green(baseColor)
    val baseBlue: Int = Color.blue(baseColor)
    val red: Int = (baseRed + mRandom.nextInt(256)) / 3
    val green: Int = (baseGreen + mRandom.nextInt(256)) / 3
    val blue: Int = (baseBlue + mRandom.nextInt(256)) / 3
    return Color.rgb(red, green, blue)
}

