package com.raafat.rea.utils

import com.raafat.rea.model.data.*
import java.io.Serializable

/**
 * Created by Raafat Alhmidi on 3/9/2021 @12:55 AM.
 */
interface ItemClickListener : Serializable {

    fun onClick(news: News) {
        //noop
    }

    fun onClick(searchTerm: SearchTerm) {
        //noop
    }

    fun onClick(city: City) {
        //noop
    }

    fun onClick(state: State) {
        //noop
    }

    fun onClick(listing: Listing) {
        //noop
    }

    fun onClick(media: Media) {
        //noop
    }
    fun onClick(listingId:String){
        //noop
    }

    fun onSave(listing:Listing){
        //noop
    }
}