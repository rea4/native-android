package com.raafat.rea.utils

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.withStyledAttributes
import androidx.navigation.NavController
import com.raafat.rea.R


/**
 * Created by Raafat Alhmidi on 3/10/2021 @11:26 PM.
 */
class CustomToolBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var firstBtnIcn: Int = 0
    private var secondBtnIcn: Int = 0
    private var closeBtnIcn: Int = 0
    private var title: String? = null

    private var firstBtn: ImageView
    private var secondBtn: ImageView
    private var closeBtn: ImageView
    private var titleTxtV: TextView

    var navController: NavController? = null
    var topBarBtnListeners: TopBarBtnListeners? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.top_bar_layout, this)
        firstBtn = findViewById(R.id.firstBtn)
        secondBtn = findViewById(R.id.secondBtn)
        closeBtn = findViewById(R.id.closeImg)
        titleTxtV = findViewById(R.id.titleTxtV)

        context.withStyledAttributes(attrs, R.styleable.CustomToolBar) {
            firstBtnIcn = getResourceId(R.styleable.CustomToolBar_firstBtnIcn, firstBtnIcn)
            secondBtnIcn = getResourceId(R.styleable.CustomToolBar_secondBtnIcn, 0)
            closeBtnIcn = getResourceId(
                R.styleable.CustomToolBar_closeBtnIcn,
                R.drawable.ic_baseline_clear_24
            )
            title = getString(R.styleable.CustomToolBar_title)
        }

        if (firstBtnIcn == 0)
            firstBtn.visibility = GONE

        if (secondBtnIcn == 0)
            secondBtn.visibility = GONE

        if (title.isNullOrBlank())
            titleTxtV.visibility = GONE

        titleTxtV.text = title
        firstBtn.setImageResource(firstBtnIcn)
        secondBtn.setImageResource(secondBtnIcn)
        closeBtn.setImageResource(closeBtnIcn)

        closeBtn.setOnClickListener {
            navController?.navigateUp()
        }

        firstBtn.setOnClickListener {
            topBarBtnListeners?.onFirstBtnClick()
        }

        secondBtn.setOnClickListener {
            topBarBtnListeners?.onSecondBtnClick()
        }

    }


    fun setFirstBtnIcon(@DrawableRes icon: Int) {
        firstBtnIcn = icon
        firstBtn.visibility = VISIBLE
        firstBtn.setImageResource(firstBtnIcn)
        firstBtn.imageTintList = null

    }

}