package com.raafat.rea.utils

/**
 * Created by Raafat Alhmidi on 3/11/2021 @12:03 AM.
 */
interface TopBarBtnListeners {

    fun onFirstBtnClick(){}
    fun onSecondBtnClick(){}
}
